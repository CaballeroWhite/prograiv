<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Document</title>
    <style>
        body{

            background-image: url(../img/fondo.jpg);
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        form{
            margin:auto;
            text-align: center;
        }

        .IngresoLogin{
            
            margin: auto;
            margin-top:10%;
            width: auto;
            height: 50%;
        }
        .btn{
            width: 50%;
            margin: auto;
        }
    </style>
</head>
<body>


<div class="content">

    <div class="IngresoLogin col-md-8">
        
                
        <form action="DatosRegistro.php" method="POST" class="col-md-4 col-xl-4">

                    <div class="form-group col-md-12">
                        <label for="">Ingrese el nombre de usuario</label>
                        <input type="text" name="UserN" id="UserN" class="form-control" required autocomplete="off">

                    </div>

                    <div class="form-group col-md-12">
                        <label for="">Ingrese una contraseña</label>
                        <input type="password" name="PassN" id="PassN" class="form-control" required >
                    </div>
                    
                    <button type="submit" class="btn btn-primary  btn-block">Registrar</button>
                    <a href="index.php">Regresar al inicio</a>

            </form>

    </div>
    
    </div>
    
</body>
</html>