-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 28, 2019 at 11:51 PM
-- Server version: 10.3.14-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id9630046_webbanco`
--

-- --------------------------------------------------------

--
-- Table structure for table `cliente`
--

CREATE TABLE `cliente` (
  `id_Cliente` int(11) NOT NULL,
  `Nombres` varchar(45) DEFAULT NULL,
  `Apellidos` varchar(45) DEFAULT NULL,
  `Sexo` varchar(45) DEFAULT NULL,
  `Edad` int(11) DEFAULT NULL,
  `Direccion` varchar(45) DEFAULT NULL,
  `Dui` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cliente`
--

INSERT INTO `cliente` (`id_Cliente`, `Nombres`, `Apellidos`, `Sexo`, `Edad`, `Direccion`, `Dui`) VALUES
(1, 'juan jose', 'serpas carranza', 'H', 19, 'santiago de maria ,usulutan', '09128347'),
(2, 'Luis Alfredo ', 'Caballero Gonzales', 'H', 20, 'santiago de maria ', '01223456'),
(3, 'carlos alberto', 'hernandez', 'H', 25, 'san miguel', '043212456'),
(4, 'Ana Gabriela', 'Castellon ', 'M', 22, 'San Salvador', '0987273753'),
(5, 'Marcela Alejandra', 'Vasquez ', 'M', 30, 'Usulutan', '092837753'),
(6, 'Diana Elizabeth', 'Orellana Argueta', 'M', 25, 'San miguel', '09328374');

-- --------------------------------------------------------

--
-- Table structure for table `cuenta`
--

CREATE TABLE `cuenta` (
  `idCuenta` int(11) NOT NULL,
  `SaldoCuenta` int(11) DEFAULT NULL,
  `ClienteId` int(11) DEFAULT NULL,
  `TransaccionId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cuenta`
--

INSERT INTO `cuenta` (`idCuenta`, `SaldoCuenta`, `ClienteId`, `TransaccionId`) VALUES
(1, 500, 3, 2),
(2, 500, 3, 3),
(3, 1000, 4, 5),
(4, 8000, 3, 4),
(5, 5000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `empleado`
--

CREATE TABLE `empleado` (
  `idEmpleado` int(11) NOT NULL,
  `NombreEMP` varchar(45) DEFAULT NULL,
  `ApellidoEMP` varchar(45) DEFAULT NULL,
  `DireccionEMP` varchar(45) DEFAULT NULL,
  `DuiEMP` varchar(45) DEFAULT NULL,
  `TelefonoEMP` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empleado`
--

INSERT INTO `empleado` (`idEmpleado`, `NombreEMP`, `ApellidoEMP`, `DireccionEMP`, `DuiEMP`, `TelefonoEMP`) VALUES
(1, 'Luis Fernando', 'Cruz Gomez', 'San miguel/ San Miguel', '0745631277', '77145235'),
(2, 'Juan Alberto', 'Perez Turcios', 'San Miguel', '142536871', '61452368'),
(3, 'Luis Fernando', 'Cruz Gomez', 'San miguel/ San Miguel', '0745631277', '77145235'),
(4, 'Juan Alberto', 'Perez Turcios', 'San Miguel', '142536871', '61452368');

-- --------------------------------------------------------

--
-- Table structure for table `prestamo`
--

CREATE TABLE `prestamo` (
  `id_prestamo` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `monto` decimal(5,5) DEFAULT NULL,
  `fecha_Prestamo` datetime DEFAULT NULL,
  `cuota_mensual` decimal(5,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transaccion`
--

CREATE TABLE `transaccion` (
  `idTransaccion` int(11) NOT NULL,
  `MontoTransaccion` int(11) DEFAULT NULL,
  `Cliente` int(11) DEFAULT NULL,
  `FechaTransaccion` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaccion`
--

INSERT INTO `transaccion` (`idTransaccion`, `MontoTransaccion`, `Cliente`, `FechaTransaccion`) VALUES
(1, NULL, 3, NULL),
(2, 200, 2, '2019-05-02 00:00:00'),
(3, 1000, 5, '2019-05-03 00:00:00'),
(4, 8000, 6, '2019-05-01 00:00:00'),
(5, 800, 4, '2019-05-02 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_Cliente`);

--
-- Indexes for table `cuenta`
--
ALTER TABLE `cuenta`
  ADD PRIMARY KEY (`idCuenta`),
  ADD KEY `TransaccionId_idx` (`TransaccionId`),
  ADD KEY `ClienteId_idx` (`ClienteId`);

--
-- Indexes for table `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`idEmpleado`);

--
-- Indexes for table `prestamo`
--
ALTER TABLE `prestamo`
  ADD PRIMARY KEY (`id_prestamo`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indexes for table `transaccion`
--
ALTER TABLE `transaccion`
  ADD PRIMARY KEY (`idTransaccion`),
  ADD KEY `Cliente_idx` (`Cliente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_Cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `cuenta`
--
ALTER TABLE `cuenta`
  MODIFY `idCuenta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `empleado`
--
ALTER TABLE `empleado`
  MODIFY `idEmpleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaccion`
--
ALTER TABLE `transaccion`
  MODIFY `idTransaccion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `ClienteId` FOREIGN KEY (`ClienteId`) REFERENCES `cliente` (`id_Cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `TransaccionId` FOREIGN KEY (`TransaccionId`) REFERENCES `transaccion` (`idTransaccion`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `prestamo`
--
ALTER TABLE `prestamo`
  ADD CONSTRAINT `prestamo_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_Cliente`);

--
-- Constraints for table `transaccion`
--
ALTER TABLE `transaccion`
  ADD CONSTRAINT `Cliente` FOREIGN KEY (`Cliente`) REFERENCES `cliente` (`id_Cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
