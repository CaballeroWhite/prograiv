<?php
require_once('lib/nusoap.php');
require_once('Funciones.php');

$server = new soap_server();

$server->configureWSDL('Prueba','urn:Prueba');
$server->wsdl->addComplexType('cuenta','complexType','struct','all','',
    array(
        'idCuenta'=>array('idCuenta'=>'idCuenta','type'=>'xsd:string'),
        'SaldoCuenta'=>array('SaldoCuenta'=>'SaldoCuenta','type'=>'xsd:string'),
        'ClienteId'=>array('ClienteId'=>'ClienteId','type'=>'xsd:string'),
        'TransaccionId'=>array('TransaccionId'=>'TransaccionId','type'=>'xsd:string')

    )
);

$server->wsdl->addComplexType('cuentaArray','complexType','array','','SOAP-ENC:Array',
    array(),
    array(
         array(
                'ref' => 'SOAP-ENC:arrayType',
                'wsdl:arrayType' => 'tns:cuenta[]'
            )
    )
);

$server->register(
    'ListarCuentas',
    array('datos'=>'xsd:string'),
    array('result'=>'xsd:bool','cuenta' => 'tns:cuentaArray','error'=> 'xsd:string'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#ListarCuentas'
);

$server->register(
    'Buscar',
    array('idCuenta'=>'xsd:string'),
    array('result'=>'xsd:bool','cuenta' => 'tns:cuentaArray','error'=> 'xsd:string'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#Buscar'
);

$server->register(
    'NuevaCuenta',
    array('idCuenta'=>'xsd:string','SaldoCuenta'=>'xsd:string','ClienteId'=>'xsd:string','TransaccionId'=>'xsd:string'),
    array('result'=>'xsd:boolean'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#NuevaCuenta'
);

$server->register(
    'actualizar',
    array('idCuenta'=>'xsd:string','SaldoCuenta'=>'xsd:string','ClienteId'=>'xsd:string','TransaccionId'=>'xsd:string'),
    array('result'=>'xsd:boolean'),
    'urn:PruebaWSDL',
    'urn:PruebaWSDL#actualizar'
);


@$server->service(file_get_contents("php://input"));


?>