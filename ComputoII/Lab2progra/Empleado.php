<?php

    Class Empleado{
        private $Nombre;
        public $Salario;
        const ISSS = 7.5;
        const AFP = 7.75;
        const Renta = 10;
        public $TotalCalculo = 0;


        function __construct(){
            $this->Nombre = "";
            $this->Salario = "";
           
        }

        public function CalculoISSS(){
            $this->TotalCalculo = ($this->Salario * self::ISSS)/100;
            return $this->TotalCalculo;

        }

        public function CalculoAFP(){
            $this->TotalCalculo = ($this->Salario * self::AFP)/100;
            return $this->TotalCalculo;

        }

        public function CalculoRenta(){
            $this->TotalCalculo = ($this->Salario * self::Renta)/100;
            return $this->TotalCalculo;

        }

        public function SalarioLiquido($AFP,$ISSS,$RENTA){
            $SumaDescuentos = $AFP + $ISSS + $RENTA;
            $SalarioFinal = $this->Salario - $SumaDescuentos;
            return $SalarioFinal;
        }
    }



?>