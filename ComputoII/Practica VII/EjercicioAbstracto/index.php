<!DOCTYPE html>
<html>

   <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="CSS/DIV.css">
        <meta charset="UTF-8">
        
        <script>
             $(function() {   

                    $('#LibrosX').change(function(){
                        $('.Libs').hide();
                        $('#' + $(this).val()).show();
                    });
             });
             $(function() {    

                    $('#Solicitar').change(function(){
                        $('.formS').hide();
                        $('#' + $(this).val()).show();
                    });
             });
             $(function() {    

                    $('#LibrosDX').change(function(){
                        $('.formS2').hide();
                        $('#' + $(this).val()).show();
                    });
                });
                                    
         </script>
   
   </head>

   <body>
   <h3>Control de prestamos de libros y revistas</h3>
           
           <div class="form-group">
                <label for="Solicitar">Selecciona una opcion:</label>
                <Select class="form-control" id="Solicitar" name="Solicitar">
                    
                    <option value="Formulario">Prestar Producto</option>
                    <option value="Consultar">Consultar Productos</option>
                    
                </Select>
            </div>

    <div id="Formulario" class="formS" style="display:none">
     
        <form method="POST" action="Prestamos.php">
        <div class="form-group">
            <label for="exampleInputEmail1">cod Producto</label>
            <input type="number" class="form-control" name="codP"  placeholder="Codigo del producto" min="1" max="5" required>
            
        </div>
        <div class="form-group">
            <label for="Titulo">Titulo</label>
            <input type="text" class="form-control" name="Titulo" placeholder="titulo" required>
        </div>
        <div class="form-group">
            <label for="Autor">Autor</label>
            <input type="text" class="form-control" name="Autor" placeholder="Autor" required>
        </div>
        <div class="form-group">
            <label for="Desc">Descripcion</label>
            <input type="text" class="form-control" name="Desc" placeholder="Descripcion del producto" required>
        </div>
        <div class="form-group">
            <label for="Desc">Editorial</label>
            <input type="text" class="form-control" name="Editorial" placeholder="Editorial" required> 
        </div>
        <div class="form-group">
            <label for="fecha">Fecha de prestamo</label>
            <input type="date" class="form-control" name="fecha" >
        </div>
        <div class="form-group">
            <label for="fechaE">Fecha de Entrega</label>
            <input type="date" class="form-control" name="fechaE" >
        </div>
        <!-- Seleccionar Revista o Libro  -->
        <div class="form-group">
            <label for="LibrosX">Selecciona el producto:</label>
            <Select class="form-control" id="LibrosX" name="LibrosX">
                
                <option value="Revistas">Revista</option>
                <option value="TableDiv">Libros</option>
                
            </Select>
        </div>
        
        <button type="submit" name="envioD" class="btn btn-primary">Guardar datos</button>
        </form>
     </div>
        

<!-- TABLA DE LIBROS -->
            <div id="TableDiv"  class="Libs" style="display:none">

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">CodProducto</th>
                            <th scope="col">Titulo</th>
                            <th scope="col">Autor</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Editorial</th>
                            <th scope="col">Año</th>

                        </tr>
                    </thead>
                    <tbody>
                        <?php

                            include 'Arrays.php';
                            echo "<tr>";
                            ALibros();
                        
                        ?>
                    </tbody>

                    <!-- TABLA DE REVISTAS -->
                </table>
            </div>

                

                <div id="Revistas" class="Libs" style="display:none">

                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">CodProducto</th>
                            <th scope="col">Titulo</th>
                            <th scope="col">Autor</th>
                            <th scope="col">Descripcion</th>
                            <th scope="col">Editorial</th>
                            <th scope="col">Año</th>

                        </tr>
                    </thead>
                    <tbody>
                    <!-- arrays-->
                        <?php

                            echo "<tr>";
                            ARevistas();

                        ?>
                    </tbody>

                   
                    </table>
                </div>

                <!---- TABLA PARA CONSULTAR CON EL SELECT -->

        <div id="Consultar"  class="formS" style="display:none">
            <!-- Seleccionar Revista o Libro  -->
            <div class="form-group">
                <label for="LibrosDX">Selecciona el producto:</label>
                <Select class="form-control" id="LibrosDX" name="LibrosDX">
                    
                    <option value="RevistasD">Revista</option>
                    <option value="LibrosD">Libros</option>
                    
                </Select>
            </div>

                <!-- TABLA DE LIBROS -->
            <div id="LibrosD"  class="formS2" style="display:none">
                    
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">CodProducto</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Editorial</th>
                        <th scope="col">Año</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                        
                        echo "<tr>";
                        ALibros();
                    
                    ?>
                </tbody>

                <!-- TABLA DE REVISTAS -->
            </table>
            </div>



            <div id="RevistasD" class="formS2" style="display:none">

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">CodProducto</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Autor</th>
                        <th scope="col">Descripcion</th>
                        <th scope="col">Editorial</th>
                        <th scope="col">Año</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                        
  
                        echo "<tr>";
                        ARevistas();

                    ?>
                </tbody>

   
    </table>
</div>
</div>
   </body>





</html>

