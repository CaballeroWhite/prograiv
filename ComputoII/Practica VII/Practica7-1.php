<?php

class areaFigura{
    protected $ancho,$largo,$resultado;

    //Obtiene losvalores iniciales para los atributos
    function __construct($v_largo,$v_ancho){
        $this->ancho=$v_ancho;
        $this->largo=$v_largo;

    }

    function __destruct(){
        echo "<br>Objeto tipo AreaFigura destruido<br>";
    }

    function calcularArea(){
        $this->resultado=$this->ancho * $this->largo;
        echo "<br>";
        return $this->resultado;
    }
}

class volumenFigura extends areaFigura{
    private $alto;
    private $volumen;
    private $var_ancho;
    private $var_largo;

    function __construct($v_alto,$v_largo,$v_ancho)
    {

        //Usu del parent para mandar datos al constructor de la clase padre.
        //Y aprovecharlo para usar el método que contiene calculararea.
        parent::__construct($v_alto,$v_largo);
        $this->alto=$v_ancho;

    }


    //Una vez que el objeto es creado, y usa alguno de sus métodos, se destruye para ahorrar memoria
    function __destruct(){
        echo "<br>Objeto VolumenFigura destruido</br>";
    }

    function calcularVolumen(){
        $this->volumen=($this->ancho)*($this->largo)*($this->alto);
        echo "<br>";
        return $this->volumen;
    }
}


$fig1 = new volumenFigura(5,2,1);
echo $fig1->calcularVolumen();

$fig2 = new areaFigura(4,6);
echo $fig2->calcularArea();





?>